#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 15:11:54 2021

@author: angelina
"""
from __future__ import absolute_import
from tree_node import TreeNode

class Tree:
    """
    The root of the binary search tree.
    """

    def __init__(self, root):
        """
        Method to initialize the tree object,
        which takes in parameter the root of the tree.

        Parameters
        ----------
        root : str
            root of the nodes.

        Returns
        -------
        None.

        """
        self.root = root


    def traversal_deep(self):
        """
        Method that traverses the tree.

        Returns
        -------
        None.

        """
        print(self.root)


    def search(self, node):
        """
        Method that looks for a specific node

        Parameters
        ----------
        node : str
            search node.

        Returns
        -------
        searched_node : str
            displays the found node.

        """
        return self.root.search_node(node)


    def add_node(self, new_node, node):
        """
        Method that integrates the add method, to add a node as a string.

        Parameters
        ----------
        new_node : str
            name of the new node to insert.
        node : str
            name of the node to which we want to insert the new node.

        Returns
        -------
        None.

        """
        node_find = self.search(node)
        self.add(new_node, node_find)


    def add(self, new_node, node):
        """
        Method to insert a new node.

        Parameters
        ----------
        new_node : str
            name of the new node to insert.
        node : str
            name of the node to which we want to insert the new node.

        Returns
        -------
        None.

        """
        added_node = TreeNode(new_node)
        if self.root is  None:
            self.root = added_node

        elif not node.is_leaf():
            if node.left_child is None:
                node.left_child = added_node
            elif node.right_child is None:
                node.right_child = added_node
            else:
                self.add(new_node, node.left_child)

        else:
            node.right_child = added_node


    def delete_node(self, node):
        """
        Method to delete a node.
        It is not yet operational, it only removes nodes that do not have children.

        Parameters
        ----------
        node : str
            name of the node to delete.

        Returns
        -------
        None.

        """
        delete_node = self.root.search_node(node)
        parent_node = self.root.search_parent(node)

        if delete_node.left_child is not None:

            if parent_node.left_child == delete_node:
                parent_node.left_child = delete_node.left_child
            else:
                parent_node.right_child = delete_node.left_child

            parent_node.insert_node(delete_node.right_child, parent_node)

        elif delete_node.right_child is not None:

            if parent_node.right_child == delete_node:
                parent_node.right_child = delete_node.right_child
            else:
                parent_node.left_child = delete_node.right_child

        else:
            if parent_node.left_child == delete_node:
                parent_node.left_child = None
            else:
                parent_node.right_child = None
