#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 14:54:00 2021

@author: angelina
"""
from __future__ import absolute_import
from tree_node import TreeNode
from tree import Tree

if __name__ == "__main__":

#  Representation:

#                          Root
#                          |
#          +---------------+---------------+
#          |                               |
#          N1                             N2
#  +-------+                       +-------+-------+
#  |                               |               |
# TP                             CM1             CM2
#                                 |               |
#                              +---+           +---+---+
#                              |               |       |
#                             Git            POO     Result

    tree = TreeNode('Root')
    arbre= Tree(tree)


    arbre.add_node('N1','Root')
    arbre.add_node('TP','N1')
    print(tree)
    print("---------------------------------------------")
    arbre.add_node('N2','Root')
    print(tree)
    print("---------------------------------------------")
    arbre.add_node('CM1', 'N2')
    arbre.add_node('CM2', 'N2')
    print(tree)
    print("---------------------------------------------")
    arbre.add_node('Git', 'CM1')
    arbre.add_node('POO', 'CM2')
    arbre.add_node('Result', 'CM2')
    print(tree)
    print("---------------------------------------------")

    print("\u26A0"*30)

    print("WARNING:the delete function only deletes nodes that have no children.")

    print("\u26A0"*30)

    arbre.delete_node('TP')
    arbre.delete_node('Result')
    print("---------------------------------------------")
    print(tree)
