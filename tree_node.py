#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 14:46:21 2021

@author: angelina

"""

class TreeNode:
    """
    The class allows you to classify a node according to its own
    position as a child (left or right) and the type of children that the node has,
    and keeps track of the parent as an attribute of each node.
    """
    def __init__(self, data):
        """
        Method to initialize the node object.

        Parameters
        ----------
        data : str
           Parent node.

        Returns
        -------
        None.

        """
        self.data = data
        self.left_child = None
        self.right_child = None
        self.level = 0


    def print_tree_node(self):
        """
        Method is used to print the tree.

        Returns
        -------
        None.

        """

        if self.right_child:
            self.right_child.print_tree_node()
        print(self.data)
        if self.left_child:
            self.left_child.print_tree_node()


    def is_leaf(self):
        """
        Method that checks if the node is a leaf.

        Returns
        -------
        bool
            True or False.

        """
        return str(self.right_child) is None and str(self.left_child) is None


    def __str__(self):
        """
        Method that displays a representation of the tree.

        Returns
        -------
        str
            [[[Result;POO]:CM2;[None;Git]:CM1]:N2;[None;TP]:N1]:Root

        Representation:

                                Root
                                |
                +---------------+---------------+
                |                               |
                N1                             N2
        +-------+                       +-------+-------+
        |                               |               |
       TP                             CM1             CM2
                                       |               |
                                    +---+           +---+---+
                                    |               |       |
                                   Git            POO     Result

        """
        if self.is_leaf():
            return str(self.data)
        return "[" + str(self.left_child) + ";" + str(self.right_child)+ "] : " + str(self.data)


    def search_node(self, node):
        """
        Method that looks for a specific node

        Parameters
        ----------
        node : str
            search node.

        Returns
        -------
        searched_node : str
            displays the found node.

        """
        if self.data is node:
            return self
        searched_node = None

        if self.left_child is not None:
            searched_node = self.left_child.search_node(node)

        if self.right_child and searched_node is None:
            searched_node = self.right_child.search_node(node)

        return searched_node


    def search_parent(self, node):
        """
        Method to search for the parent node

        Parameters
        ----------
        node : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        if self.left_child is not None and self.left_child.data is node:
            return self

        if self.right_child is not None and self.right_child.data is node:
            return self

        searched_node = None

        if self.left_child is not None:
            searched_node = self.left_child.search_parent(node)

        if searched_node is None and self.right_child is not None:
            searched_node = self.right_child.search_parent(node)

        return searched_node
